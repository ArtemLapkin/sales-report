<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link               
 * @since             1.0.0
 * @package           SKM_Sales_Report
 *
 * @wordpress-plugin
 * Plugin Name:       SKM Sales Report
 * Plugin URI:         
 * Description:       Sales Report
 * Version:           1.0.0
 * Author:             
 * Author URI:         
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       skm-sales-report
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-skm-sales-report-activator.php
 */
function activate_skm_sales_report() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-skm-sales-report-activator.php';
	Skm_Sales_Report_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-skm-sales-report-deactivator.php
 */
function deactivate_skm_sales_report() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-skm-sales-report-deactivator.php';
	Skm_Sales_Report_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_skm_sales_report' );
register_deactivation_hook( __FILE__, 'deactivate_skm_sales_report' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-skm-sales-report.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_skm_sales_report() {

	$plugin = new SKM_Sales_Report();
	$plugin->run();

}
run_skm_sales_report();
