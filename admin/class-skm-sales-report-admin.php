<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link        
 * @since      1.0.0
 *
 * @package    SKM_Sales_Report
 * @subpackage SKM_Sales_Report/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    SKM_Sales_Report
 * @subpackage SKM_Sales_Report/admin
 * @author       < >
 */
class SKM_Sales_Report_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	private $main_page_slug;
	private $settings_page_slug;
	private $create_report_page_slug;
	private $all_reports_page_slug;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->main_page_slug = 'skm-sales-report';
        $this->settings_page_slug = $this->main_page_slug . '-settings';
        $this->create_report_page_slug = $this->main_page_slug . '-create';
        $this->all_reports_page_slug = $this->main_page_slug . '-all';

    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in SKM_Sales_Report_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The SKM_Sales_Report_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        if( $this->is_page_with_styles_scripts() ) {
            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/skm-sales-report-admin.css', array(), $this->version, 'all');
            // jquery datepicker
            wp_enqueue_style( 'skm-datepicker', plugin_dir_url( __FILE__ ) . 'assets/jquery-datepicker/jquery-ui.css', null, false );
            // jquery confirm master
            wp_enqueue_style( 'skm-jquery-confirm', plugin_dir_url( __FILE__ ) . 'assets/jquery-confirm-master/jquery-confirm.min.css', null, false );

        }
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in SKM_Sales_Report_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The SKM_Sales_Report_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
        if( $this->is_page_with_styles_scripts() ){
            // jquery datepicker
            wp_enqueue_script( 'skm-datepicker', plugin_dir_url( __FILE__ ) . 'assets/jquery-datepicker/jquery-ui.js', array( 'jquery' ), null, false );
            // jquery confirm master
            wp_enqueue_script( 'skm-jquery-confirm', plugin_dir_url( __FILE__ ) . 'assets/jquery-confirm-master/jquery-confirm.min.js', array( 'jquery' ), null, false );

            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/skm-sales-report-admin.js', array( 'skm-datepicker', 'skm-jquery-confirm' ), null, false ); //$this->version
        }
	}

    /**
     *
     * @return bool
     */
    private function get_admin_page_id() {
	    $out = false;
	    if( function_exists( 'get_current_screen' ) ) {
            $screen = get_current_screen();
            $out = $screen->id;
        }

        return $out;
	}


    /**
     *
     * @return bool
     */
    private function is_page_with_styles_scripts() {
        $screen_id = $this->get_admin_page_id();

        return $screen_id === 'sales-report_page_skm-sales-report-create' || $screen_id === 'sales-report_page_skm-sales-report-all';
	}

    /**
     * Creates WP-Admin pages
     */
    public function report_page() {
        add_menu_page(__('Sales Report', 'skm-sales-report'), __('Sales Report', 'skm-sales-report'), 'manage_options', $this->main_page_slug );
        // New Report page
        add_submenu_page($this->main_page_slug, __('New Report', 'skm-sales-report'), __('New Report', 'skm-sales-report'), 'manage_options', $this->create_report_page_slug, array( $this, 'create_report_page' ));
        // All Reports page
        add_submenu_page($this->main_page_slug, __('All Reports', 'skm-sales-report'), __('All Reports', 'skm-sales-report'), 'manage_options', $this->all_reports_page_slug, array( $this, 'all_reports_page' ));
        // Settings page
        add_submenu_page($this->main_page_slug, __('Settings', 'skm-sales-report'), __('Settings', 'skm-sales-report'), 'manage_options', $this->settings_page_slug, array( $this, 'settings_page' ));
        // Hide first one submenu
        remove_submenu_page($this->main_page_slug, $this->main_page_slug);
	}

    /**
     * New Report page
     */
    public function create_report_page() {
        $last_monday_unix = strtotime('last Sunday', time());
        $last_monday = date('m/d/Y', strtotime('last Monday', time()));
        $tuesday_before_monday = date('m/d/Y', strtotime('last Tuesday', $last_monday_unix));
        ?>
        <div class="wrap">
            <h1><?php _e('New Report', 'skm-sales-report'); ?></h1>
            <form action="<?php menu_page_url($this->all_reports_page_slug, true); ?>" method="post" class="skm-form-generator skm-mgt-20 skm-p-rel">
                <div class="skm-third">
                    <label for="date-start" class="skm-smaller-text">Start Date</label>
                    <input type="text" name="date-start" id="skm-js-date-start" class="skm-js-date" autocomplete="off" readonly="readonly" value="<?php echo $tuesday_before_monday; ?>">
                </div>
                <div class="skm-third">
                    <label for="date-end" class="skm-smaller-text">End Date</label>
                    <input type="text" name="date-end" id="skm-js-date-end" class="skm-js-date" autocomplete="off" readonly="readonly" value="<?php echo $last_monday; ?>" >
                </div>
                <div class="skm-third">
                    <span class="skm-js-products-found skm-products-found"><?php _e('Select date range to process', 'skm-sales-reports'); ?></span>
                </div>
                <div class="skm-clearfix"></div>
                <div class="skm-button-wrapper skm-ta-right">
                    <input type="hidden" name="skm_create_report" value="1">
                    <?php wp_nonce_field( SKM_Sales_Report_Helper::$nonce); ?>
                    <label for="skm-xls" class="skm-format-label"><input type="radio" name="format" id="skm-xls" value="xls" checked=""><?php _e('XLS', 'skm-sales-reports'); ?></label>
                    <label for="skm-txt" class="skm-format-label"><input type="radio" name="format" id="skm-txt" value="txt"><?php _e('TXT', 'skm-sales-reports'); ?></label>
                    <button class="button button-primary" disabled="disabled" id="skm-js-submit-form"><?php _e('Export', 'skm-sales-reports'); ?></button>
                </div>
                <?php $this->print_loader(); ?>
            </form>
        </div>
        <?php
	}

    /**
     * All Reports page
     */
    public function all_reports_page() {
        $report_created = SKM_Sales_Report_Helper::perform_submit();
        $reports = SKM_Sales_Report_Helper::get_sales_reports($report_created); ?>
        <div class="wrap">
            <h1><?php _e('Reports', 'skm-sales-report'); ?></h1>
            <?php
            if( !empty( $reports ) ) {
                $this->print_reports_table($reports);
            } else {
                ?>
                <p class="skm-mgt-20"><?php _e('No Reports yet', 'skm-sales-report'); ?></p>
                <?php
            }
            ?>
        </div>
        <?php
	}

    /**
     * Prints all reports table
     * @param $reports
     */
    private function print_reports_table($reports) {
        ?>
        <div class="skm-p-rel skm-all-reports skm-mgt-20">
            <div class="skm-js-reports">
                <table class="widefat striped skm-js-reports">
                    <thead>
                    <tr>
                        <th scope="col" id="title" class="manage-column column-title column-primary sortable desc">
                            <span class="skm-table-title"><?php _e('Title', 'skm-sales-report'); ?></span>
                        </th>
                    </tr>
                    </thead>

                    <tbody id="the-list">
                    <?php
                    foreach ($reports as $title => $data) {
                        $class = isset( $data['highlighted'] ) ? 'class="skm-new-added"' : '';
                        ?>
                        <tr <?php echo $class ?>>
                            <td class="title column-title has-row-actions column-primary page-title" data-colname="Title">
                                <strong>
                                    <a class="row-title" href="<?php echo $data['url']; ?>" download="<?php echo $data['title']; ?>"><?php echo $data['title']; ?></a>
                                </strong>
                                <div class="row-actions">
                                    <span class="edit">
                                        <a href="<?php echo $data['url']; ?>" download="<?php echo $data['title']; ?>"><?php _e('Download', 'skm-sales-report'); ?></a> |
                                    </span>
                                    <span class="inline hide-if-no-js trash">
                                        <a href="#" class="trash skm-js-delete-report" data-path="<?php echo $data['path']; ?>" aria-label="delete"><?php _e('Delete', 'skm-sales-report'); ?></a>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
                <div class="skm-button-wrapper">
                    <button class="button button-danger button-large skm-js-delete-all-reports"><?php _e('Delete all reports', 'skm-sales-report'); ?></button>
                </div>
            </div>
            <?php $this->print_loader(); ?>
            <p class="skm-hidden skm-js-no-reports-message skm-mgt-20"><?php _e('No reports yet', 'skm-sales-report'); ?></p>
        </div>
        <?php
	}
	
    /**
     * Settings Page
     */
    public function settings_page() {
        $post_code_type =  esc_attr(get_option('skm_post_code_type'));
        ?>
        <div class="wrap">
            <h1><?php _e('Settings', 'skm-sales-report'); ?></h1>
            <form method="post" action="options.php">
                <?php settings_fields( 'skm-sales-reports-group' ); ?>
                <?php do_settings_sections( 'skm-sales-reports-group' ); ?>
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row"><?php _e('Account Number', 'skm-sales-reports'); ?></th>
                        <td><input type="text" name="skm_account_number"
                                   value="<?php echo esc_attr(get_option('skm_account_number')); ?>"/></td>
                    <tr valign="top">
                        <th scope="row"><?php _e('Post Code Type', 'skm-sales-reports'); ?></th>
                        <td>
                            <select name="skm_post_code_type" id="skm_post_code_type">
                                <option value="billing" <?php selected($post_code_type, 'billing', true); ?>><?php _e('Billing', 'skm-sales-reports') ?></option>
                                <option value="shipping" <?php selected($post_code_type, 'shipping', true); ?>><?php _e('Shipping', 'skm-sales-reports') ?></option>
                            </select>
                        </td>
                </table>
                <?php submit_button(); ?>
            </form>
        </div>
        <?php
	}

    /**
     * Prints loader div
     */
    private function print_loader() {
        ?>
        <div class="skm-loader-wrapper skm-js-loader">
            <div class="skm-loader"></div>
        </div>
        <?php
	}

    /**
     * Ajax, removes report
     */
    public function ajax_skm_remove_report() {
        // only admin restriction
        if( ! current_user_can('manage_options') ) {
            wp_send_json(array('status'=>false));
        }

        $path = isset($_POST['path']) && trim($_POST['path']) !== ''  ? trim($_POST['path']) : '';

        if( $path === '' ) {
            wp_send_json(array('status'=>false));
        }

        $res = SKM_Sales_Report_Helper::remove_report($path);
        wp_send_json(array('status'=>$res));
    }

    /**
     * Ajax, removes all reports
     */
    public function ajax_skm_remove_all_reports() {
        // only admin restriction
        if( ! current_user_can('manage_options') ) {
            wp_send_json(array('status'=>false));
        }

        $res = SKM_Sales_Report_Helper::remove_all_reports();
        wp_send_json(array('status'=>$res));
    }

    /**
     * Calculates amount of products in orders date range
     */
    public function ajax_skm_search_orders() {
        $date_start = isset( $_POST['date-start'] ) ? $_POST['date-start'] : false;
        $date_end   = isset( $_POST['date-end'] ) ? $_POST['date-end'] : false;

        // if both dates are not passed - send error
        if( ! $date_start || ! $date_end ) {
            wp_send_json(array('status' => false, 'span_text' => __('Start Date or End date is not passed', 'skm-sales-reports')));
        }

        // get product IDs for selected date range
        $count = SKM_Sales_Report_Helper::count_products_in_date_range($date_start, $date_end);
        if( $count !== 0 ) {
            $span_text = sprintf( _n( '%s product found', '%s products found', $count, 'skm-sales-reports' ), $count );;
        } else {
            $span_text = __('No products found', 'skm-sales-reports');
        }

        wp_send_json(array('status' => true, 'products_count' => $count, 'span_text' => $span_text ));
    }

    /**
     * Register plugin setting
     */
    public function register_setting() {
        register_setting( 'skm-sales-reports-group', 'skm_account_number' );
        register_setting( 'skm-sales-reports-group', 'skm_post_code_type' );
    }

    /**
     * Adds meta box to edit product page
     */
    public function add_meta_boxes_for_product() {
        add_meta_box(SKM_Sales_Report_Helper::$meta_name, __('Is Digital Product', 'skm_account_number'), array($this, 'meta_box_html'), 'product', 'side', 'high', null);
    }

    /**
     * Prints HTML for meta box
     * @param $post
     */
    public function meta_box_html($post) {
        wp_nonce_field(basename(__FILE__), 'skm-meta-box-nonce');
        $is_digital = get_post_meta($post->ID, SKM_Sales_Report_Helper::$meta_name, true);
        ?>
        <div>
            <label for="<?php echo SKM_Sales_Report_Helper::$meta_name; ?>"><input name="<?php echo SKM_Sales_Report_Helper::$meta_name; ?>" id="<?php echo SKM_Sales_Report_Helper::$meta_name; ?>" type="checkbox" value="1" <?php checked($is_digital, 1, true); ?>><?php _e('Yes', 'skm-sales-reports'); ?></label>
        </div>
        <?php
    }

    /**
     * Stores meta box
     * @param $post_id
     * @return mixed
     */
    public function save_meta_boxes_for_product($post_id) {

        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return $post_id;
        }

        if ( 'product' !== $_POST['post_type'] || ! current_user_can( 'edit_product', $post_id ) ) {
            return $post_id;
        }

        if ( !wp_verify_nonce( $_POST['skm-meta-box-nonce'], basename(__FILE__) ) ) {
            return $post_id;
        }

        $old = get_post_meta( $post_id, SKM_Sales_Report_Helper::$meta_name, true );

        $new = isset( $_POST[SKM_Sales_Report_Helper::$meta_name]) ? $_POST[SKM_Sales_Report_Helper::$meta_name] : '';
        if ( $new && $new !== $old ) {
            update_post_meta( $post_id, SKM_Sales_Report_Helper::$meta_name, $new );
        } elseif ( '' === $new && $old ) {
            delete_post_meta( $post_id, SKM_Sales_Report_Helper::$meta_name, $old );
        }
    }
}
