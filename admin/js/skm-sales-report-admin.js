(function( $ ) {
	'use strict';
	$(document).ready(function (){
        // create report page
	    if( $('.skm-js-date').length !== 0 ) {
            $('#skm-js-date-start').datepicker({
                altFormat: "yy-mm-dd",
                maxDate: new Date,
                onSelect: function () {
                    var datepickerEnd = $('#skm-js-date-end'),
                        minDate = $(this).datepicker('getDate');
                    datepickerEnd.datepicker('option', 'minDate', minDate);
                    if( datepickerEnd.val() !== '' ) {
                        searchOrders();
                    }
                }
            });

            $('#skm-js-date-end').datepicker({
                altFormat: "yy-mm-dd",
                maxDate: new Date,
                onSelect: function () {
                    var datepickerStart = $('#skm-js-date-start'),
                        maxDate = $(this).datepicker('getDate');
                    datepickerStart.datepicker('option', 'maxDate', maxDate);
                    if( datepickerStart.val() !== '' ) {
                        searchOrders();
                    }
                }
            });

            // additional check
            $('.skm-js-date').change(function (){
                var values = [];
                $('.skm-js-date').each(function (){
                    values.push($(this).val());
                });
                if( values.indexOf('') !== -1 ){
                    $('#skm-js-submit-form').attr('disabled', true);
                }
            });
            searchOrders();
        }

        // all reports page
        if( $('.skm-all-reports').length !== 0 ) {
            $('.skm-js-delete-report').click(function (e){
                e.preventDefault();
                var $this = $(this),
                    path = $this.data('path'),
                    tr = $this.parents('tr'),
                    loader = $('.skm-js-loader');
                $.confirm({
                    title: 'Warning!',
                    content: 'Are you sure you want to delete report?',
                    buttons: {
                        confirm: function () {
                            if( path === '' ){
                                $.alert('Error! File path is empty');
                                return;
                            }
                            loader.show();
                            $.ajax({
                                type: "POST",
                                data: {
                                    action: 'skm_remove_report',
                                    path: path,
                                },
                                url: ajaxurl,
                                success: function ( response ) {
                                    if( response.status ) {
                                        prettyRemoveEl(tr);
                                    }
                                    loader.hide();
                                },
                            })
                        },
                        cancel: function () {

                        }
                    }
                });
            });

            $('.skm-js-delete-all-reports').click(function (e){
                e.preventDefault();
                var loader = $('.skm-js-loader');
                $.confirm({
                    title: 'Warning!',
                    content: 'Are you sure you want to delete all reports?',
                    buttons: {
                        confirm: function () {
                            loader.show();
                            $.ajax({
                                type: "POST",
                                data: {
                                    action: 'skm_remove_all_reports',
                               },
                                url: ajaxurl,
                                success: function ( response ) {
                                    if( response.status ) {
                                        $('.skm-js-reports').hide();
                                        $('.skm-js-no-reports-message').show();
                                    }
                                    loader.hide();
                                },
                            })
                        },
                        cancel: function () {

                        }
                    }
                })
            });
        }

        // Pretty remove el
        function prettyRemoveEl(tr){
            tr.addClass('skm-going-to-remove');
            setTimeout(function (){
                tr.fadeOut(400, function (){
                    $(this).remove();
                })
            }, 500)
        }

        // Search orders in date range
        function searchOrders() {
	        var dateStart = $('#skm-js-date-start').val(),
                dateEnd =  $('#skm-js-date-end').val();

	        if(dateStart === '' && dateEnd === '' ) {
	            return;
            }

            var loader = $('.skm-js-loader'),
                submitButton = $('#skm-js-submit-form'),
                span = $('.skm-js-products-found');
            loader.show();
            $.ajax({
                type: "POST",
                data: {
                    action: 'skm_search_orders',
                    'date-start': dateStart,
                    'date-end': dateEnd
                },
                url: ajaxurl,
                success: function ( response ) {
                    if( response.status ) {
                        if(response.products_count !== 0 ) {
                            submitButton.attr('disabled', false);
                        } else {
                            submitButton.attr('disabled', true);
                        }
                    } else {
                        // if start or end date is not passed
                        submitButton.attr('disabled', true);
                    }
                    span.text(response.span_text);
                    loader.hide();
                },
            })
        }
	})
})( jQuery );
