<?php

/**
 * Fired during plugin deactivation
 *
 * @link        
 * @since      1.0.0
 *
 * @package    SKM_Sales_Report
 * @subpackage SKM_Sales_Report/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    SKM_Sales_Report
 * @subpackage SKM_Sales_Report/includes
 * @author       < >
 */
class SKM_Sales_Report_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
