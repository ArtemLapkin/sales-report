<?php
/**
 * Helper for generating sales reports
 *
 * @since      1.0.0
 * @package    SKM_Sales_Report
 * @subpackage SKM_Sales_Report/includes
 * @author       < >
 */
class SKM_Sales_Report_Helper {

    public static $nonce = 'skm-generate-report';
    private static $excel_library_loader = '/XLSXWriter/xlsxwriter.class.php';
    private static $uploads_folder = 'exported-reports';
    static $meta_name = 'akm-is-digital-product';

    /**
     * Performs form submit
     * @return bool|mixed
     */
    public static function perform_submit() {
        if( ! isset( $_POST['skm_create_report'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce'], SKM_Sales_Report_Helper::$nonce ) ) return false;

        $date_start = isset( $_POST['date-start'] ) ? $_POST['date-start'] : false;
        $date_end   = isset( $_POST['date-end'] ) ? $_POST['date-end'] : false;

        if( ! $date_start || ! $date_end ) return false;

        $format = isset( $_POST['format'] ) ? $_POST['format'] : 'xls';

        // get order IDs for selected date range
        $orders_ids = self::get_order_ids($date_start, $date_end);
        // get all orders products
        $orders_data = self::get_orders_data($orders_ids);

        if( empty( $orders_data ) ) {
            return false;
        }

        if( $format === 'xls' ) {
            // prepare clean data for XLS
            $data = self::prepare_clean_data($orders_data, $date_end);
            // create XLS file
            return self::create_xls($data, array($date_start, $date_end));
        } else {
            // prepare clean data for TXT
            $data = self::prepare_clean_data($orders_data, $date_end, 'txt');
            // create TXT file
            return self::create_txt($data, array($date_start, $date_end));
        }
    }

    /**
     * Determines what order statuses are used in report
     * @return array
     */
    private static function get_order_statuses() {
        // this way will get all orders
        //$woo_statuses = wc_get_order_statuses();
        //array_keys($woo_statuses);
        return array('wc-processing', 'wc-completed');
    }

    /**
     * @param bool $date_start
     * @param bool $date_end
     * @return mixed
     */
    private static function get_order_ids($date_start, $date_end) {
        // start day - 12:00am end day 11:59pm
        $date_start_full =  date( 'Y-m-d H:i:s', strtotime( $date_start ) );
        $date_end_full = date( 'Y-m-d H:i:s', strtotime( $date_end ) + 24 * 60 * 60 - 1  );

        $args = array(
            'post_type'  => 'shop_order',
            'post_status' => self::get_order_statuses(),
            'posts_per_page' => -1,
            'fields' => 'ids',
            'date_query' => array(
                array(
                    'after' => $date_start_full,
                    'before' => $date_end_full
                )
            )
        );
        $query = new WP_Query($args);

        return $query->posts;
    }

    /**
     * @param $order_ids
     * @param bool $products_count
     * @return array|int
     */
    private static function get_orders_data($order_ids, $products_count = false) {
        $out = $products_count === false ? array() : 0;
        if( ! empty( $order_ids ) ) {
            foreach ($order_ids as $order_id) {
                $order = wc_get_order( $order_id );

                foreach( $order->get_items() as $item_id => $item ) {
                    $product = $item->get_product();
                    // skip if this is not Digital Product
                    if( 1 !=  get_post_meta( $product->get_id(), self::$meta_name, true ) ) {
                        continue;
                    }

                    if( $products_count === false ) {
                        $out[$order_id]['status'] = self::payment_status($order->get_status());
                        $out[$order_id]['products'][$product->get_sku()] = $item->get_quantity();
                        if( self::get_post_code_type() === 'shipping' ){
                            $out[$order_id]['post_code'] = $order->get_shipping_postcode();
                        } else {
                            $out[$order_id]['post_code'] = $order->get_billing_postcode();
                        }
                    } else {
                        $out += $item->get_quantity();
                    }
                }
            }
        }
        return $out;
    }

    /**
     * @param $date_start
     * @param $date_end
     * @return array
     */
    public static function count_products_in_date_range($date_start, $date_end) {
        $orders_ids = self::get_order_ids($date_start, $date_end);
        $count = self::get_orders_data($orders_ids, true);

        return $count;
    }
    
    /**
     * Returns Order payment status code S/R
     * @param $status
     * @return string
     */
    private static function payment_status($status) {
        switch ($status) {
            case 'completed':
            case 'processing':
                $out = 'S'; // sale
            break;
            case 'refund':
                $out = 'R'; // refund
            break;
            default:
                $out = 'X'; // todo what is for other orders that are not S or R
        }
        return $out;
    }

    /**
     * Prepares data for XLS
     * @param $order_data
     * @param $date_end
     * @param string $format
     * @return array|string
     */
    private static function prepare_clean_data($order_data, $date_end, $format='xls') {

        if( empty( $order_data ) ) {
            return array();
        }

        $start_ind = '92';
        $start_chain = '4030';
        $account_number = self::get_account_number();
        $date_end_formatted = date( 'ymd',strtotime($date_end));

        $sales_header_record = $start_ind . $start_chain . $account_number . $date_end_formatted;


        if( $format === 'xls' ) {
            $out = array(
                'head' => array('Strata', 'Barcode (13 digits)', 'Zipcode', 'Sale or Return?', 'Combined', '', 'Account', 'Date', 'Sales Header Record')
            );
        } else {
            $out = $sales_header_record . "\r\n";
        }

        $sales_products = $sales_orders = 0;
        foreach ($order_data as $order_datum) {
            foreach ($order_datum['products'] as $product_sku => $quantity) {
                if( $order_datum['status'] === 'S' ) {
                    $sales_orders++;
                }
                // new line for each product
                for ($i = 1; $i<= $quantity; $i++) {
                    if( $order_datum['status'] === 'S' ) {
                        $sales_products++;
                    }
                    // todo R orders exist?
                    //else if( $order_datum['status'] === 'R' ) {
                    //    $sales_orders++;
                    //}
                    if( $format === 'xls' ) {
                        $out['products'][] = array(
                            'M3', $product_sku, $order_datum['post_code'], $order_datum['status'], 'M3'
                        );
                    } else {
                        $out .= 'M3' . $product_sku . $order_datum['post_code'] . $order_datum['status'] . "\r\n";
                    }
                }
            }
        }
        $sales_products_six_dig = str_pad($sales_products, 6, '0', STR_PAD_LEFT);
        $sales_orders_six_dig = str_pad($sales_orders, 6, '0', STR_PAD_LEFT);
        $sales_trailer_record = '94' . $sales_orders_six_dig . $sales_products_six_dig;

        if( $format === 'xls' ) {
            for ($i = 0; $i<=2; $i++) {
                if(!isset( $out['products'][$i] )) {
                    $out['products'][$i] = array('','','','','');
                }
            }
            $out['products'][0] = array_merge($out['products'][0], array('', $account_number, $date_end_formatted, $sales_header_record . ' '));
            $out['products'][1] = array_merge($out['products'][1], array('', 'Total Sales Records', 'Net Units', 'Sales Trailer Record'));
            $out['products'][2] = array_merge($out['products'][2], array('', $sales_orders_six_dig, $sales_products_six_dig, $sales_trailer_record. ' '));

        } else {
            $out .= $sales_trailer_record;
        }

        return $out;
    }

    /**
     * Creates XLS report
     * @param $data
     * @param $date_range
     * @return bool|string
     */
    private static function create_xls($data, $date_range) {
        // no products passed
        if( empty( $data ) ) return false;

        $excel_library_file = plugin_dir_path( __FILE__ ) . self::$excel_library_loader;
        // missing Excel Library
        if( ! file_exists( $excel_library_file) ) return false;
        require_once $excel_library_file;

        $writer = new XLSXWriter();

        if( isset($data['head']) ) {
            $writer->writeSheetRow('Sheet1', $data['head'] );
        }

        if( isset( $data['products'] ) && is_array( $data['products'] ) ) {
            foreach ($data['products'] as $product) {
                $writer->writeSheetRow('Sheet1', $product );
            }
        }

        $file_new = self::prepare_file_name($date_range);
        
        $writer->writeToFile($file_new['path']);

        return $file_new['name'];
    }

    /**
     * Creates TXT file
     * @param $data
     * @param $date_range
     * @return bool|mixed
     */
    private static function create_txt($data, $date_range) {

        if($data === '') return false;

        $file_new = self::prepare_file_name($date_range, 'txt');

        $file = fopen($file_new['path'],'wb');
        fwrite($file,$data);
        $res = fclose($file);
        if( $res !== false ){
            return $file_new['name'];
        } else {
            return false;
        }
    }

    /**
     * @param $date_range
     * @param string $format
     * @return array
     */
    private static function prepare_file_name($date_range, $format = 'xlsx') {

        $uploads_folder = self::get_uploads_folder_path();

        $file_name = str_replace('/', '-', implode(' - ', $date_range));
        $file_name_ext = $file_name . '.' . $format;
        $path = $uploads_folder . '/'.$file_name_ext;

        $out = array(
            'name' => $file_name_ext,
            'path' => $path
        );

        return $out;
    }

    /**
     * Prints success message
     * @param $message
     */
    private static function print_success_message($message) {
        ?>
        <div class='notice notice-success is-dismissible'>
            <p><?php echo $message; ?></p>
        </div>
        <?php
    }

    /**
     * Returns uploads folder path
     * @return string
     */
    private static function get_uploads_folder_path() {
        $uploads_folder = plugin_dir_path( __DIR__ ) . self::$uploads_folder . '/';
        if( ! file_exists($uploads_folder) ){
            mkdir($uploads_folder, 0755, true);
        }
        return $uploads_folder;
    }

    /**
     * Returns uploads folder url
     * @return string
     */
    private static function get_uploads_folder_url() {
        return plugin_dir_url( __DIR__ ) . self::$uploads_folder . '/';
    }

    /**
     * Returns list of created reports
     * @return array
     */
    public static function get_sales_reports($created_report) {

        if( $created_report !== false ) {
            self::print_success_message(sprintf( esc_html__( 'Report -  "%1$s" was successfully created', 'skm-sales-report' ), $created_report ));
        }

        $uploads_url = self::get_uploads_folder_url();
        $uploads_path = self::get_uploads_folder_path();
        $files = self::get_all_files_in($uploads_path);
        $out = array();
        foreach ($files as $item) {
            $created_time = filemtime($uploads_path . $item);
            $out[$created_time]['path'] = $uploads_path . $item;
            $out[$created_time]['url'] = $uploads_url . $item;
            $out[$created_time]['title'] = $item;
            if( $created_report !== false && $created_report === $item ) {
                $out[$created_time]['highlighted'] = true;
            }
        }
        // sort by created date
        krsort($out);
        return $out;
    }


    private static function get_all_files_in($path) {
        $files = array_diff(scandir($path), array('..', '.'));

        return $files;
    }

    /**
     * Removes catalog
     * @param $path
     * @return bool
     */
    public static function remove_report($path) {
        return unlink($path);
    }

    /**
     * Removes all reports
     * @return bool
     */
    public static function remove_all_reports() {
        $uploads_path = self::get_uploads_folder_path();
        $files = self::get_all_files_in($uploads_path);
        $res = array();
        foreach ($files as $file) {
            $res[] = unlink($uploads_path . $file);
        }
        // some file wasn't removed
        if( in_array(false, $res) ) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns option that describes what post code to use, shipping or billing
     * @return string
     */
    private static function get_post_code_type() {
        $post_code_type = get_option('skm_post_code_type');
        if( $post_code_type === '' ) {
            $post_code_type = 'billing';
        }

        return $post_code_type;
    }

    /**
     * @return mixed
     */
    private static function get_account_number() {
        return esc_attr(get_option('skm_account_number'));
    }
}
